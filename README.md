---
page_type: sample
languages:
- nodejs
- javascript
products:
- azure
- azure-app-service
description: "This sample demonstrates a tiny Hello World Node.js app for Azure App Service."
---

# Node.js Hello World

This sample demonstrates a tiny Hello World node.js app for [App Service Web App](https://docs.microsoft.com/azure/app-service-web).

## Credits

Original Authors: [Microsoft's Azure Samples Team](https://github.com/orgs/Azure-Samples/people)

This project was forked from the following location: [node-js-docs-hello-world](https://github.com/Azure-Samples/nodejs-docs-hello-world)